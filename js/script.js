// import Swiffy Slider JS
import { swiffyslider } from 'swiffy-slider'
window.swiffyslider = swiffyslider;

window.addEventListener("load", () => {
    window.swiffyslider.init();
});

// import Swiffy Slider CSS
import "swiffy-slider/css"

(function ($) { 
  $(window).on("scroll", function () {
      if ($(".sticky-menu").length) {
        var headerScrollPos = 130;
        var stricky = $(".sticky-menu");
        if ($(window).scrollTop() > headerScrollPos) {
          stricky.addClass("sticky-fixed");
        } else if ($(this).scrollTop() <= headerScrollPos) {
          stricky.removeClass("sticky-fixed");
        }
      }
      if ($(".scroll-to-top").length) {
        var strickyScrollPos = 100;
        if ($(window).scrollTop() > strickyScrollPos) {
          $(".scroll-to-top").fadeIn(700);
        } else if ($(this).scrollTop() <= strickyScrollPos) {
          $(".scroll-to-top").fadeOut(700);
        }
      }


    });
})(jQuery);
